# MLaaS-DB

Database microservice for MLaaS application.

## Prerequisites

    go get "github.com/minio/minio-go"

A minio database running somewhere. Consider using [mlaas-compose](https://github.com/cwpearson/mlaas-compose), which launches minio, OpenZipkin, and etcd with `docker-compose`.