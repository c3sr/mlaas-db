package mlaasdb

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"io"
	"strconv"
	"sync"

	log "bitbucket.org/c3sr/p3sr-logger"

	tracing "bitbucket.org/c3sr/p3sr-trace"
	"github.com/minio/minio-go"
)

const (
	dataBucket = "data"

	location = "us-east-1"
)

type Client struct {
	minioClient *minio.Client
	mutex       *sync.Mutex
}

var std *Client

func StandardClient() *Client {
	return std
}

func NewClient(endpoint, accessKeyID, secretAccessKey string, useSSL bool) (*Client, error) {
	minioClient, err := minio.New(endpoint, accessKeyID, secretAccessKey, useSSL)
	return &Client{minioClient, &sync.Mutex{}}, err

}

func (c *Client) ensureBucket(ctx context.Context, bucketName string) error {
	sp, _ := tracing.StartSpanFromContext(ctx, "ensure_bucket")
	defer sp.Finish()

	err := c.minioClient.MakeBucket(bucketName, location)

	if err != nil {
		// Check to see if we already own this bucket (which happens if you run this twice)
		_, existsErr := c.minioClient.BucketExists(bucketName)
		if existsErr != nil {
			return existsErr
		}
	}
	return nil
}

func getDataPrefixForUser(userID string) string {
	return userID
}

func (c *Client) listObjects(bucket, prefix string, isRecursive bool) ([]string, error) {
	ret := []string{}
	doneCh := make(chan struct{})
	defer close(doneCh)
	objectCh := c.minioClient.ListObjects(bucket, prefix, isRecursive, doneCh)
	for object := range objectCh {
		if object.Err != nil {
			return nil, object.Err
		}
		ret = append(ret, object.Key)
	}
	return ret, nil
}

func (c *Client) listData(userID string) ([]string, error) {
	isRecursive := true
	return c.listObjects(dataBucket, getDataPrefixForUser(userID), isRecursive)
}

func (c *Client) storeBinary(ctx context.Context, bucket, name string, bin []byte) (context.Context, error) {
	err := c.ensureBucket(ctx, bucket)
	if err != nil {
		log.WithError(err).Error("Unable to store binary. Name: ", name, " Bucket: ", bucket)
		return ctx, err
	}

	sp, ctx := tracing.StartSpanFromContext(ctx, "store_binary")
	_, err = c.minioClient.PutObject(bucket, name, bytes.NewReader(bin), "application/octet-stream")
	sp.Finish()
	if err != nil {
		log.WithError(err).Error("Unable to put object")
		return ctx, err
	}
	log.Debug("Stored ", len(bin), " bytes at ", bucket, ":", name)
	return ctx, err
}

type storeBinaryArgs struct {
	bucket string
	name   string
	bin    []byte
}

func (c *Client) storeBinaries(ctx context.Context, reqs []storeBinaryArgs) error {
	errs := make(chan error, 1)
	finished := make(chan bool, 1)
	var wg sync.WaitGroup

	for _, req := range reqs {
		bucket := req.bucket
		name := req.name
		bin := req.bin
		wg.Add(1)
		go func() {
			defer wg.Done()
			_, err := c.storeBinary(ctx, bucket, name, bin)
			if err != nil {
				errs <- err
			}
		}()
	}

	go func() {
		wg.Wait()
		close(finished)
	}()

	select {
	case <-finished:
	case err := <-errs:
		if err != nil {
			return err
		}
	}

	return nil
}

func (c *Client) getBinary(ctx context.Context, bucket, name string) ([]byte, error) {
	sp, ctx := tracing.StartSpanFromContext(ctx, "get_binary")
	defer sp.Finish()

	minioSpan, _ := tracing.StartSpanFromContext(ctx, "minio_get_object")
	object, err := c.minioClient.GetObject(bucket, name)
	if err != nil {
		log.WithError(err).Error("Couldn't get object at: ", bucket, name)
		return nil, err
	}
	minioSpan.Finish()

	copySpan, _ := tracing.StartSpanFromContext(ctx, "copy_object")
	buf := bytes.NewBuffer([]byte{})
	_, err = io.Copy(buf, object)
	if err != nil {
		return nil, err
	}
	copySpan.Finish()
	log.Debug("Retrieved ", len(buf.Bytes()), " bytes at ", bucket, ":", name)
	return buf.Bytes(), nil
}

type getBinaryArgs struct {
	bucket string
	name   string
}

func (c *Client) getBinaries(ctx context.Context, args []getBinaryArgs) ([][]byte, error) {

	type getBinaryResult struct {
		bytes []byte
		err   error
	}

	var mutex sync.Mutex
	var wg sync.WaitGroup
	ret := make([][]byte, len(args))
	errs := make(chan error, 1)
	finished := make(chan bool, 1)

	for i, arg := range args {
		bucket := arg.bucket
		name := arg.name

		wg.Add(1)
		go func(i int) {
			defer wg.Done()
			bytes, err := c.getBinary(ctx, bucket, name)
			if err != nil {
				errs <- err
			}
			mutex.Lock()
			defer mutex.Unlock()
			ret[i] = bytes
		}(i)
	}

	go func() {
		wg.Wait()
		close(finished)
	}()

	select {
	case <-finished:
	case err := <-errs:
		if err != nil {
			return nil, err
		}
	}

	return ret, nil
}

func (c *Client) StoreData(userID string, data []byte) (string, error) {
	err := c.ensureBucket(context.TODO(), dataBucket)
	if err != nil {
		log.WithError(err).Error("Unable to store data")
		return "", err
	}

	dataID := "data.bin"
	dataPath := getDataPrefixForUser(userID) + "/" + dataID

	//name := objectName(userid, dataid)
	_, err = c.storeBinary(context.TODO(), dataBucket, dataPath, data)
	if err != nil {
		return "", err
	}
	//log.Info("Stored '", name, "' (size ", n, ") in bucket '", dataBucket, "'")

	return dataID, nil
}

func StoreData(userid string, data []byte) (string, error) {
	return std.StoreData(userid, data)
}

func (c *Client) GetData(ctx context.Context, userID, dataID string) ([]byte, error) {
	dataPath := getDataPrefixForUser(userID) + "/" + dataID
	return c.getBinary(ctx, dataBucket, dataPath)
}

func GetData(ctx context.Context, userid, dataid string) ([]byte, error) {
	return std.GetData(ctx, userid, dataid)
}

func (c *Client) getNewModelID(ctx context.Context, userID string) (string, error) {

	sp, ctx := tracing.StartSpanFromContext(ctx, "new_model_id")
	defer sp.Finish()

	models, err := c.ListModels(ctx, userID)

	if err != nil {
		return "", err
	}
	fmt.Println(models)

	for i := 0; true; i++ {
		testModelName := "model" + strconv.Itoa(i)
		free := true
		for _, model := range models {
			if model == testModelName { // name is taken
				free = false
				break
			}
		}
		if free {
			return testModelName, nil
		}
	}
	log.Fatal("Should not get here")
	return "", errors.New("Unable to find new name")
}

func (c *Client) StoreModel(ctx context.Context, userID string, pb, ckpt, meta []byte) (string, context.Context, error) {
	c.mutex.Lock()
	defer c.mutex.Unlock()

	modelID, err := c.getNewModelID(ctx, userID)
	if err != nil {
		log.WithError(err).Error("Error getting new model ID")
		return "", ctx, err
	}
	log.Debug("Generated new modelID: ", modelID)
	modelPrefix := getModelPrefix(userID, modelID)
	log.Debug("Using prefix: ", modelPrefix)

	pbName := pbSuffix
	ckptName := ckptSuffix
	metaName := metaSuffix

	reqs := []storeBinaryArgs{{modelsBucket, modelPrefix + "/" + pbName, pb},
		{modelsBucket, modelPrefix + "/" + ckptName, ckpt},
		{modelsBucket, modelPrefix + "/" + metaName, meta}}

	err = c.storeBinaries(ctx, reqs)

	return modelID, ctx, err
}

func StoreModel(ctx context.Context, userid string, pb, ckpt, meta []byte) (string, context.Context, error) {
	return std.StoreModel(ctx, userid, pb, ckpt, meta)
}

func (c *Client) GetModel(ctx context.Context, userID, modelID string) (pb, ckpt, meta []byte, err error) {
	modelPrefix := getModelPrefix(userID, modelID)

	pbName := modelPrefix + "/" + pbSuffix     //objectName(userid, modelid+pbSuffix)
	ckptName := modelPrefix + "/" + ckptSuffix //objectName(userid, modelid+ckptSuffix)
	metaName := modelPrefix + "/" + metaSuffix // objectName(userid, modelid+metaSuffix)

	args := []getBinaryArgs{
		{modelsBucket, pbName},
		{modelsBucket, ckptName},
		{modelsBucket, metaName},
	}

	bins, err := c.getBinaries(ctx, args)
	if err != nil {
		log.WithError(err).Error("Couldn't get binaries for ", modelsBucket, ":", pbName, " ", ckptName, " ", metaName)
		return nil, nil, nil, err
	}

	return bins[0], bins[1], bins[2], nil
}

func GetModel(ctx context.Context, userid, modelid string) (pb, ckpt, meta []byte, err error) {
	return std.GetModel(ctx, userid, modelid)
}

func init() {
	// endpoint := "kastra.ddns.net:9000"
	// accessKeyID := "AHOSTY89ZXSH9W34PF66"
	// secretAccessKey := "dg4hgZBDW2HpdgNLycaLZgJ15xp2zkey9sNAdd5I"
	// endpoint := "localhost:9000"
	// accessKeyID := "YTK6SJBMI1IHM59QV0DC "
	// secretAccessKey := "u9A7+9oouv3SbCOA42qc/5fPHVHSyLTfa00vCcnd"
	endpoint := "corev1.csl.illinois.edu:9000"
	accessKeyID := "AKIAIOSFODNN7EXAMPLE"
	secretAccessKey := "wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY"
	useSSL := false
	var err error
	std, err = NewClient(endpoint, accessKeyID, secretAccessKey, useSSL)
	log.Info("Started connection to minio at ", endpoint)
	if err != nil {
		panic(err)
	}
}
