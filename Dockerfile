FROM golang
MAINTAINER Carl Pearson

# Get glide
RUN curl https://glide.sh/get | sh

# Install deps
ADD . /mlaas-db
WORKDIR /mlaas-db
RUN glide install

# Run the service when docker starts
ENTRYPOINT go run mlaas-db/cmd/main.go
