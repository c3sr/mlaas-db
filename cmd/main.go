package main

import (
	service "bitbucket.org/c3sr/mlaas-db/service"
	config "bitbucket.org/c3sr/p3sr-config"
	tracing "bitbucket.org/c3sr/p3sr-trace"
	log "github.com/Sirupsen/logrus"
)

func main() {
	config.Init() // initialize etcd and so forth

	// Create a new mlaas-db service object that uses the minio database client
	s := service.New()

	tracer, closer := tracing.New("mlaas-db")
	defer closer.Close()
	tracing.InitGlobalTracer(tracer)

	err := s.Start()
	if err != nil {
		log.WithError(err).Fatal("Unable to start mlaas-db service")
	}

}
