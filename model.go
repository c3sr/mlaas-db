package mlaasdb

import "path"

const (
	modelsBucket = "models"

	pbSuffix   = "__pb"
	ckptSuffix = "__ckpt"
	metaSuffix = "__meta"
)

// type model struct {
// 	id             string
// 	user           string
// 	pb, ckpt, meta []byte
// }

// func (m *model) getPrefix() (bucket, name string) {
// 	return modelsBucket, m.user + "/" + m.id
// }

func getModelPrefix(userID, modelID string) string {
	return getModelsPrefixForUser(userID) + "/" + modelID
}

func getModelsPrefixForUser(userID string) string {
	return userID
}

func getModelFromName(object string) string {
	dir := path.Dir(object) // cut the object name off
	return path.Base(dir)   // return only the model part of the path
	// return path.Dir(object)
}
