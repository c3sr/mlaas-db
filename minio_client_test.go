package mlaasdb

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestStoreData(t *testing.T) {

	_, err := StoreData("test_user", []byte("test payload"))
	if err != nil {
		t.Error(err)
	}
}

func TestStoreThenGetData(t *testing.T) {

	payload := []byte("test payload")

	dataid, err := StoreData("test_user", payload)
	if err != nil {
		t.Error(err)
	}

	data, err := GetData("test_user", dataid)
	if err != nil {
		t.Error(err)
	}

	assert.Equal(t, data, payload, "payloads should be equal")
}

func TestStoreModel(t *testing.T) {

	_, err := StoreModel("test_user", []byte("model payload"))
	if err != nil {
		t.Error(err)
	}
}

func TestStoreThenGetMOdel(t *testing.T) {

	payload := []byte("test model")

	modelid, err := StoreModel("test_user", payload)
	if err != nil {
		t.Error(err)
	}

	model, err := GetModel("test_user", modelid)
	if err != nil {
		t.Error(err)
	}

	assert.Equal(t, model, payload, "payloads should be equal")
}
