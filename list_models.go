package mlaasdb

import (
	"context"

	tracing "bitbucket.org/c3sr/p3sr-trace"
)

func ListModels(ctx context.Context, userID string) ([]string, error) {
	return std.ListModels(ctx, userID)
}

func (c *Client) ListModels(ctx context.Context, userID string) ([]string, error) {
	c.ensureBucket(ctx, modelsBucket)
	isRecursive := true
	modelNames := map[string]struct{}{}
	sp, _ := tracing.StartSpanFromContext(ctx, "list_objects")
	objectNames, err := c.listObjects(modelsBucket, getModelsPrefixForUser(userID), isRecursive)
	sp.Finish()
	if err != nil {
		return nil, err
	}
	for _, name := range objectNames {
		modelNames[getModelFromName(name)] = struct{}{}
	}

	ret := []string{}
	for model := range modelNames {
		ret = append(ret, model)
	}
	return ret, nil
}
