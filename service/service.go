package mlassdbservice

import (
	"golang.org/x/net/context"
	"google.golang.org/grpc"

	minio "bitbucket.org/c3sr/mlaas-db"
	services "bitbucket.org/c3sr/mlaas-services"
	pg "bitbucket.org/c3sr/p3sr-grpc"
	log "bitbucket.org/c3sr/p3sr-logger"
	service "bitbucket.org/c3sr/p3sr-services/service"
)

type Service struct {
	*service.Base
	server *grpc.Server
}

func New() *Service {
	desc := services.DbServiceDescription
	return &Service{Base: service.NewBase(desc),
		server: pg.NewServer(desc),
	}
}

func (s *Service) Start() error {
	return service.Start(s)
}

func (s *Service) GetServer() *grpc.Server {
	return s.server
}

func (s *Service) StoreData(c context.Context, in *services.StoreDataRequest) (*services.StoreDataReply, error) {
	log.Info("StoreData called")
	id, err := minio.StoreData(in.Userid, in.Data)
	if err != nil {
		log.WithError(err).Error("Error storing data")
	}
	return &services.StoreDataReply{Id: id}, err
}

func (s *Service) GetData(ctx context.Context, in *services.GetDataRequest) (*services.BinaryData, error) {
	data, err := minio.GetData(ctx, in.Userid, in.Dataid)
	if err != nil {
		log.WithError(err).Error("Error retrieving data")
	}
	return &services.BinaryData{Data: data}, err
}

func (s *Service) StoreModel(c context.Context, in *services.StoreModelRequest) (*services.StoreModelReply, error) {
	id, c, err := minio.StoreModel(c, in.Userid, in.Model.GetPb(), in.Model.GetCkpt(), in.Model.GetCkptmeta())
	if err != nil {
		log.WithError(err).Error("Error storing model")
	}
	return &services.StoreModelReply{Modelid: id}, err
}

func (s *Service) GetModel(ctx context.Context, in *services.ModelRequest) (*services.GetModelReply, error) {
	pb, ckpt, meta, err := minio.GetModel(ctx, in.Userid, in.Modelid)
	if err != nil {
		log.WithError(err).Error("Error retrieving model")
	}
	model := &services.TfModel{Pb: pb, Ckpt: ckpt, Ckptmeta: meta}
	return &services.GetModelReply{Model: model}, err
}

func (s *Service) ListModels(c context.Context, in *services.ListModelsRequest) (*services.StringsMessage, error) {
	models, err := minio.ListModels(c, in.Userid)
	if err != nil {
		log.WithError(err).Error("Error listing model")
	}

	return &services.StringsMessage{Strings: models}, err
}

func (s *Service) DeleteModel(ctx context.Context, in *services.ModelRequest) (*services.DeleteModelReply, error) {
	err := minio.DeleteModel(in.Userid, in.Modelid)
	if err != nil {
		log.WithError(err).Error("Error deleting model")
	}
	return &services.DeleteModelReply{Message: "success"}, err
}
