package mlaasdb

import (
	"fmt"

	log "bitbucket.org/c3sr/p3sr-logger"
)

func DeleteModel(userID, modelID string) error {
	return std.DeleteModel(userID, modelID)
}

func (c *Client) DeleteModel(userID, modelID string) error {
	log.Debug("")
	objectsCh := make(chan string)

	go func() {
		defer close(objectsCh)

		pbObjectName := getModelPrefix(userID, modelID) + "/" + pbSuffix
		objectsCh <- pbObjectName
		log.Debug("Added ", pbObjectName, "to channel")
		objectsCh <- getModelPrefix(userID, modelID) + "/" + ckptSuffix
		objectsCh <- getModelPrefix(userID, modelID) + "/" + metaSuffix
	}()

	errorCh := c.minioClient.RemoveObjects(modelsBucket, objectsCh)
	for e := range errorCh {
		fmt.Println("Error detected during deletion: " + e.Err.Error())
	}

	return nil
}
